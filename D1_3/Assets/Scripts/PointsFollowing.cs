﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsFollowing : MonoBehaviour
{
    [SerializeField] private Transform[] points;

    private int pointIndex = 0;

    private void Start()
    {
        transform.LookAt(points[pointIndex].position);
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(points[pointIndex].position);
        if (Vector3.Distance(points[pointIndex].position, transform.position) < 0.00000001f) pointIndex++;
    }
}

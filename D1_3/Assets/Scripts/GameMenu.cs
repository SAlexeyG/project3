﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainPamel;

    [SerializeField] private GameObject pausePanel;

    [SerializeField] private Button pauseButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private Button resumeButton;

    // Start is called before the first frame update
    void Start()
    {
        mainPamel.SetActive(true);
        pausePanel.SetActive(false);
        
        exitButton.onClick.AddListener(OnExitButton);
        pauseButton.onClick.AddListener(delegate { OnPause(true); });
        resumeButton.onClick.AddListener(delegate { OnPause(false); });
    }

    private void OnPause(bool IsPause)
    {
        Time.timeScale = IsPause ? 0f : 1f;

        mainPamel.SetActive(!IsPause);
        pausePanel.SetActive(IsPause);
    }

    private void OnExitButton()
    {
        SceneManager.LoadScene("MainMenu");
    }
}

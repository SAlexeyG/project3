﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public enum InputCommand
{
    Fire,
    Skill,
	Deadshot,
	
}

public class InputController : MonoBehaviour
{
    public static Action<InputCommand> OnInputAction;

    [SerializeField] private Button fireButton;
    [SerializeField] private Button skillButton;
	[SerializeField] private Button deadshotButton;

	private void Awake()
	{
        fireButton.onClick.AddListener(OnFireButton);
        skillButton.onClick.AddListener(OnSkillButton);
		deadshotButton.onClick.AddListener(OnDeadShotButton);
	}

	private void OnDeadShotButton()
	{
		OnInputAction?.Invoke(InputCommand.Deadshot);
	}
	
    private void OnFireButton()
	{
        OnInputAction?.Invoke(InputCommand.Fire);
	}

    private void OnSkillButton()
	{
        OnInputAction?.Invoke(InputCommand.Skill);
	}

	// Start is called before the first frame update
	void Start()
    {
        var playerInput = GetComponent<PlayerInput>();
        playerInput.onActionTriggered += OnInputTriggered;
    }

    private void OnInputTriggered(InputAction.CallbackContext context)
	{
        if(!context.performed) return;

		switch (context.action.name)
		{
            case "FireAction":
                OnFireButton();
                break;

            case "SkillAction":
                OnSkillButton();
                break;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using UnityEngine;

public class SkeletonAI : MonoBehaviour
{
    [SerializeField] private Node rootNode;
    [SerializeField] private Animator animator;

	private void Start()
	{
		Health health = GetComponent<Health>();

		health.OnDieAction += delegate { animator.SetTrigger("Die"); DelayRun.Execute(delegate { Destroy(gameObject); }, 2f, gameObject); };
		health.OnHitAction += delegate { animator.SetTrigger("Hurt"); DelayRun.Execute(delegate { }, 2f, gameObject); };
	}

	private void OnDestroy()
	{
		Health health = GetComponent<Health>();

		health.OnDieAction -= delegate { animator.SetTrigger("Die"); DelayRun.Execute(delegate { Destroy(gameObject); }, 2f, gameObject); };
		health.OnHitAction -= delegate { animator.SetTrigger("Hurt"); };
	}

	// Update is called once per frame
	void Update()
    {
        rootNode.Evaluate();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonDie : Node
{
    [SerializeField] private Animator animator;
    [SerializeField] private Health health;

    public override NodeState Evaluate()
    {
        if (health.HP <= 0) return NodeState.Success;
        else return NodeState.Failure;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemSkill : Node
{
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject ice;
    
    private float lastTime = 0f;
    
    public override NodeState Evaluate()
    {
        if (Time.time - lastTime < 4f) return NodeState.Failure;

        lastTime = Time.time;
        animator.SetTrigger("Hurt");
        StartCoroutine(Skill());

        return NodeState.Success;
    }

    private IEnumerator Skill()
    {
        for (int i = 0; i < 3; i++)
        {
            var obj = Instantiate(ice);
            obj.GetComponent<Rigidbody>().AddForce(1, 0f, 1f);
            yield return new WaitForSeconds(0.5f);
        }
    }
}

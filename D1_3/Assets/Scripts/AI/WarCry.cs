﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarCry : Node
{
    [SerializeField] private Animator animator;
    
    private enum myState
    {
        Wait,
        WarCry,
        Complete,
        
    }

    private myState state;
    private Coroutine warCryCoroutine;
    
    public override NodeState Evaluate()
    {
        switch (state)
        {
            case myState.Wait:
                state = myState.WarCry;
                warCryCoroutine = StartCoroutine(WaitWarCry());
                return NodeState.Running;
            
            case myState.WarCry:
                if (warCryCoroutine != null) return NodeState.Running;
                else
                {
                    state = myState.Complete;
                    return NodeState.Success;
                }
            
            case myState.Complete:
                return NodeState.Failure;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private IEnumerator WaitWarCry()
    {
        animator.SetTrigger("WarCry");
        yield return new WaitForSeconds(1.5f);
        warCryCoroutine = null;
    }
}

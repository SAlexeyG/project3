﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    [SerializeField] private Image image;

    // Update is called once per frame
    void Update()
    {
        float HP = GetComponent<Health>().HP;

        image.fillAmount = HP * 33.33f / 100;
    }
}

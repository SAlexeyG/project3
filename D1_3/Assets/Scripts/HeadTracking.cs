﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeadTracking : MonoBehaviour
{
	public enum Target
	{
		Player,
		Ball,
		Container,

	}

	[SerializeField] private Target target;
	[SerializeField] private Vector3 angleFix = new Vector3(180f, 0f, 90f);

	private Transform targetTransform;
	
	// Start is called before the first frame update
	void Start()
	{
		if (target == Target.Player)
			targetTransform = FindObjectOfType<PlayerController>().transform;
	}

	private void Update()
	{
		var ContainerObjects = FindObjectsOfType<BonusContainer>();
		var BallObjects = FindObjectsOfType<Ball>();

		float minDistanceToContainerObject = (ContainerObjects.Length > 0) ? ContainerObjects.Min((x) => Vector3.Distance(x.gameObject.transform.position, transform.position)) : float.MaxValue;
		float minDistanceToBallObject = (BallObjects.Length > 0) ? BallObjects.Min((x) => Vector3.Distance(x.gameObject.transform.position, transform.position)) : float.MaxValue;
		float distanceToPlayerObject = Vector3.Distance(FindObjectOfType<PlayerController>().transform.position, transform.position);

		if (minDistanceToBallObject < minDistanceToContainerObject)
		{
			target = Target.Ball;
			targetTransform = BallObjects.FirstOrDefault((x) => Vector3.Distance(x.gameObject.transform.position, transform.position) == minDistanceToBallObject).transform;
		}
		else if(minDistanceToContainerObject < distanceToPlayerObject)
		{
			target = Target.Container;
			targetTransform = ContainerObjects.FirstOrDefault((x) => Vector3.Distance(x.gameObject.transform.position, transform.position) == minDistanceToContainerObject).transform;
		}
		else
		{
			target = Target.Player;
			targetTransform = FindObjectOfType<PlayerController>().transform;
		}
	}

	// Update is called once per frame
	void LateUpdate()
	{
		Quaternion quaternion = Quaternion.LookRotation(targetTransform.position - transform.position);

		if (quaternion.eulerAngles.y < 90f) 
			quaternion.eulerAngles = new Vector3(quaternion.eulerAngles.x, 90f, quaternion.eulerAngles.z);

		if (quaternion.eulerAngles.y > 270f) 
			quaternion.eulerAngles = new Vector3(quaternion.eulerAngles.x, 270f, quaternion.eulerAngles.z);

		transform.rotation = quaternion;
		transform.Rotate(angleFix);
	}
}

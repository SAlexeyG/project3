﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemSpawner : MonoBehaviour
{
    [SerializeField] private GameObject golem;
    
    private IEnumerator Start()
    {
        for (int i = 0; i < 2; i++)
        {
            var obj = Instantiate(golem);
            obj.transform.position = new Vector3(6, 0, 20);
            obj.transform.eulerAngles = new Vector3(0, 180, 0);
            yield return new WaitForSeconds(4f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class BonusContainer : MonoBehaviour
{
	[SerializeField] private Health health;
	[SerializeField] private GameObject bonus;

	private void Reset()
	{
		health = GetComponent<Health>();
	}

	// Start is called before the first frame update
	void Start()
	{
		if (health != null) health.OnDieAction += OnDie;
	}

	private void OnDie()
	{
		if (bonus != null)
		{
			Instantiate(bonus, transform.position, transform.rotation);
			Destroy(health.gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class LevelGanerator : MonoBehaviour
{
	[SerializeField] [Min(4)] int height;
	[SerializeField] [Min(4)] int width;

	[SerializeField] GameObject[] platforms;
	[SerializeField] GameObject[] walls;
	[SerializeField] GameObject leftCorner;
	[SerializeField] GameObject rightCorner;
	[SerializeField] GameObject gates;

	[ContextMenu("Generate")]
	public void Generate()
	{
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
			{
				var obj = Instantiate(platforms[Random.Range(0, platforms.Length)], transform);
				obj.transform.localPosition = new Vector3(i * 4, 0, j * 4);
			}

		for (int i = 0; i < height - 1; i++)
		{
			var wall1 = Instantiate(walls[Random.Range(0, walls.Length)], transform);
			wall1.transform.localPosition = new Vector3(0, 0, i * 4);
			wall1.transform.Rotate(new Vector3(0f, 90f, 0f));

			var wall2 = Instantiate(walls[Random.Range(0, walls.Length)], transform);
			wall2.transform.localPosition = new Vector3((width - 1) * 4, 0, i * 4);
			wall2.transform.Rotate(new Vector3(0f, -90f, 0f));
		}

		var corner1 = Instantiate(leftCorner, transform);
		corner1.transform.localPosition = new Vector3(0f, 0f, (height - 1) * 4f);

		var corner2 = Instantiate(rightCorner, transform);
		corner2.transform.localPosition = new Vector3((width - 1) * 4, 0f, (height - 1) * 4f);

		var arch = Instantiate(gates, transform);
		arch.transform.localPosition = new Vector3((width - 1) * 2, 0f, (height - 1) * 4f + 2f);

		for (int i = 1; i < width - 1; i++)
			if (Mathf.Abs(i * 4 - arch.transform.localPosition.x) >= 4)
			{
				var wall3 = Instantiate(walls[Random.Range(0, walls.Length)], transform);
				wall3.transform.localPosition = new Vector3(i * 4, 0f, (height - 1) * 4f + 2f);
			}

		for (int i = 0; i < 2; i++)
		{
			var plat1 = Instantiate(platforms[Random.Range(0, platforms.Length)], transform);
			plat1.transform.localPosition = new Vector3(arch.transform.localPosition.x, 0f, (height + i) * 4);
		}
	}
}

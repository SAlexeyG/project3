﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Level/Data", order = 1)]
public class LevelMeshesData : ScriptableObject
{
    [SerializeField] private SerializableMeshInfo[] meshesInfo;
    public SerializableMeshInfo[] MeshesInfo => meshesInfo;

    public void SetupData(GameObject obj)
    {
        var data = new List<SerializableMeshInfo>();
        foreach (var renderer in obj.transform.GetComponentsInChildren<MeshRenderer>())
        {
            var sharedMaterial = renderer.sharedMaterial;
            var target = renderer.gameObject;
            var sharedMesh = target.GetComponent<MeshFilter>().sharedMesh;
            
            var meshInfo = new SerializableMeshInfo(obj.name, sharedMesh, sharedMaterial);
            data.Add(meshInfo);
        }

        meshesInfo = data.ToArray();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TutorialStepAbstractUI : MonoBehaviour, ITutorialStep
{
    [SerializeField] protected int id;

    public int Id => id;
    
    public virtual bool InitStep()
    {
        return TutorialController.Instance.Add(this);
    }

    public virtual void StartStep() { }

    public virtual void StopStep()
    {
        TutorialController.Instance.Stop(id);
    }

    protected void Awake()
    {
        if (!InitStep()) Destroy(this);
    }
}

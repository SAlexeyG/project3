﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] float m_Speed = 1f;
    [SerializeField] Camera m_Camera;
    [SerializeField] Text Score;
    [SerializeField] Text HPtext;

    private float strafe;
    public float Strafe => strafe;

    private float screenCenter;

    public int score;
    public int HP;

    // Start is called before the first frame update
    void Start()
    {
        screenCenter = Screen.width * 0.5f;
        HP = 9;
        //HPtext.text = (HP / 3).ToString();
        Score.text = (score / 2).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if((transform.position.z / 8) > (score / 2 + 1))
        {
            if(HP == 0) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            HP--;
            HPtext.text = (HP / 3).ToString();
        }


        if (!Input.GetMouseButton(0))
        {
            transform.Translate(new Vector3(0f, 0f, 1f) * Time.deltaTime * m_Speed);

        }
        else
        {
            float mousePos = Input.mousePosition.x;

            if (mousePos > screenCenter)
            {
                strafe = (mousePos - screenCenter) / screenCenter;
            }
            else
            {
                strafe = 1 - mousePos / screenCenter;
                strafe *= -1f;
            }

            transform.Translate(new Vector3(strafe * 1f, 0f, 1f) * Time.deltaTime * m_Speed);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Gates>() != null)
        {
            var obstacle = other.gameObject.GetComponent<Gates>();
            score++;
            Score.text = (score / 2).ToString();
        }
    }

   
}

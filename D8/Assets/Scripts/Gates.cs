﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gates : MonoBehaviour
{
    int direction;

    // Start is called before the first frame update
    void Start()
    {
        direction = Random.Range(-1, 1);
        direction = (direction == 0) ? 1 : -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > 1) direction = -1;
        if (transform.position.x < -1) direction = 1;

        transform.Translate(new Vector3(direction, 0f, 0f) * Time.deltaTime);
    }
}

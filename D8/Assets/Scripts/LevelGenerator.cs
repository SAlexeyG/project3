﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
	[SerializeField] GameObject m_Platform;
	[SerializeField] GameObject m_Gates;

	private List<GameObject> platforms = new List<GameObject>();
	private List<GameObject> gates = new List<GameObject>();

	// Start is called before the first frame update
	void Start()
	{
		platforms.Add(m_Platform.gameObject);
		for (int i = 0; i < 10; i++)
		{
			GameObject obj;

			obj = Instantiate(m_Platform, transform);
			obj.name = $"Platform {i}";

			Vector3 pos = Vector3.zero;

			pos.z = 8 * (i + 1);
			obj.transform.position = pos;

			platforms.Add(obj);

			obj = Instantiate(m_Gates, transform);
			obj.name = $"Gates {i}";

			pos = Vector3.zero;

			pos.z = 8 * (i + 1);
			pos.y = 0.24f;
			pos.x = Random.Range(-1f, 1f);
			obj.transform.position = pos;

			gates.Add(obj);
		}
	}

	// Update is called once per frame
	void Update()
	{

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using ColorSnake;
using UnityEngine;

public class ColorSnake_LevelGenerator : MonoBehaviour
{
    [SerializeField] private ColorSnake_Types m_Types;
    [SerializeField] private ColorSnake_GameController m_Controller;
    [SerializeField] private GameObject m_ColorChanger;

    private int line = 1; // номер генерируемого чанка
    private List<GameObject> obstacles = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {
        var upBorder = m_Controller.Bounds.Up;

        while (line * 2 < upBorder + 2f)
        {
            GenerateObstacle();
        }
    }

    // Update is called once per frame
    void Update()
    {
        var upBorder = m_Controller.Bounds.Up + m_Controller.Camera.transform.position.y;
        if(line * 2 > upBorder + 2) return;
        
        GenerateObstacle();
        if (line % 3 == 0)
        {
            var colorChanger = new GameObject(name: $"ColorChanger_{line}");

            var obj = Instantiate(m_ColorChanger);
            obj.transform.position = new Vector3(0f, line * 4, 0f);
            obj.transform.parent = colorChanger.transform;

            var colorType = m_Types.GetRandomColorType();
            obj.GetComponent<SpriteRenderer>().color = colorType.Color;
            obj.AddComponent<ColorSnake_ColorChanger>().ColorId = colorType.Id;
        }
        //TODO уничтожение нижних объектов
    }

    private void GenerateObstacle()
    {
        var template = m_Types.GetRandomTemplate();
        var obstacle = new GameObject(name: $"Obstacle_{line}");

        foreach (var point in template.Points)
        {
            var objType = m_Types.GetRandomObjectType();
            var colorType = m_Types.GetRandomColorType();

            var obj = Instantiate(objType.Object, point.position, point.rotation);
            obj.transform.parent = obstacle.transform;

            obj.GetComponent<SpriteRenderer>().color = colorType.Color;

            var obstacleComponent = obj.AddComponent<ColorSnake_Obstacle>();
            obstacleComponent.ColorId = colorType.Id;
            obstacleComponent.IsRotating = (Random.Range(0, 2) == 1) ? true : false;
        }

        Vector3 pos = obstacle.transform.position;
        pos.y = line * 2;

        obstacle.transform.position = pos;

        line++;
        
        obstacles.Add(obstacle);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.NetworkInformation;
using ColorSnake;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColorSnake_Snake : MonoBehaviour
{
	[SerializeField] private ColorSnake_GameController m_GameController;
	[SerializeField] private SpriteRenderer m_SpriteRenderer;

	private int currentType;
	private Vector3 position;
	private int Score = 0;

	// Start is called before the first frame update
	void Start()
	{
		position = transform.position;

		var colorType = m_GameController.Types.GetRandomColorType();
		currentType = colorType.Id;
		m_SpriteRenderer.color = colorType.Color;
	}

	// Update is called once per frame
	void Update()
	{
		position = transform.position;

		if (!Input.GetMouseButton(0)) return;

		position.x = m_GameController.Camera.ScreenToWorldPoint(Input.mousePosition).x;

		position.x = Mathf.Clamp(position.x, m_GameController.Bounds.Left, m_GameController.Bounds.Right);

		transform.position = position;
	}

	private void SetupColor(int id)
	{
		var colorType = m_GameController.Types.GetColorType(id);
		currentType = colorType.Id;
		m_SpriteRenderer.color = colorType.Color;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.GetComponent<ColorSnake_Obstacle>() != null)
		{
			var obstacle = other.gameObject.GetComponent<ColorSnake_Obstacle>();
			if (obstacle.ColorId == currentType)
			{
				Destroy(obstacle.gameObject);
				Score++;
			}
			else
			{
				print($"Game over. Score: {Score}");
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
		}
		
		if (other.gameObject.GetComponent<ColorSnake_ColorChanger>() != null)
		{
			var obstacle = other.gameObject.GetComponent<ColorSnake_ColorChanger>();
			SetupColor(obstacle.ColorId);
			Destroy(obstacle.gameObject);
		}
	}
}

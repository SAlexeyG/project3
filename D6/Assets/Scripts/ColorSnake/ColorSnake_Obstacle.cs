using UnityEngine;

namespace ColorSnake
{
    public class ColorSnake_Obstacle : MonoBehaviour
    {
        public int ColorId;
        public bool IsRotating;

        private void Update()
        {
            if(IsRotating)
				transform.Rotate(0f, 0f, 4 * Time.deltaTime);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rigidbody;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            rigidbody.bodyType = RigidbodyType2D.Dynamic;
    }
}

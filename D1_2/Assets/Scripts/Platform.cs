﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Platform : MonoBehaviour
{
	[SerializeField] private Sprite m_StartElement;
	[SerializeField] private Sprite m_MidElement;
	[SerializeField] private Sprite m_EndElement;
	[SerializeField] [Min(3)] private int m_Count;

	[SerializeField] private bool IsMoving = false;
	
	Vector2 startPosition;
	Vector2 direction;

#if UNITY_EDITOR

	[ContextMenu("Generate Platform")]
	void GeneratePlatform()
	{
		Vector2 position = this.transform.position;
		gameObject.GetComponent<SpriteRenderer>().sprite = m_StartElement;
		float size = gameObject.GetComponent<Collider2D>().bounds.size.x;
		Transform parent = this.transform.root;

		position.x += size;

		for(int i = 0; i < m_Count - 2; i++)
		{
			var obj = Instantiate(this, parent);
			obj.transform.position = position;
			obj.GetComponent<SpriteRenderer>().sprite = m_MidElement;

			position.x += size;
		}

		var endObj = Instantiate(this, parent);
		endObj.transform.position = position;
		endObj.GetComponent<SpriteRenderer>().sprite = m_EndElement;
	}

#endif

	// Start is called before the first frame update
	void Start()
	{
		startPosition = transform.position;
		direction = Vector2.right;
	}

	// Update is called once per frame
	void Update()
	{
		if (transform.position.x > startPosition.x + 3f) direction = Vector2.left;
		if (transform.position.x < startPosition.x - 3f) direction = Vector2.right;

		if(IsMoving)transform.Translate(direction * Time.deltaTime);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		bool isMovementObject = collision.transform.GetComponent<CharacterMovement>();

		if(isMovementObject)
			collision.transform.parent = transform;
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if(collision.transform.parent == transform)
			collision.transform.parent = null;
	}
}

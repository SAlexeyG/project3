﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    private static string nextLevel;
    [SerializeField] private GameObject m_Fadeout;

    public static void LoadLevel(string level)
    {
        nextLevel = level;
        SceneManager.LoadScene("LoadingScene");
    }

    private IEnumerator Start()
    {
        GameManager.SetGameState(GameState.Loading);

        yield return new WaitForSeconds(5f);

        if(string.IsNullOrEmpty(nextLevel))
        {
            for (int i = 0; i < 100; i++)
            {
                yield return new WaitForSeconds(0.01f);
                m_Fadeout.GetComponent<UnityEngine.UI.Image>().color = new Color(0f, 0f, 0f, i / 100f);
            }

            SceneManager.LoadScene("MainMenuScene");
            yield break;
        }

        AsyncOperation loading = null;

        loading = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);

        while(!loading.isDone)
            yield return null;

        for (int i = 0; i < 100; i++)
        {
            yield return new WaitForSeconds(0.01f);
            m_Fadeout.GetComponent<UnityEngine.UI.Image>().color = new Color(0f, 0f, 0f, i / 100f);
        }

        nextLevel = null;
        SceneManager.UnloadSceneAsync("LoadingScene");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class StaticObject : MonoBehaviour, IHitBox
{
    [SerializeField] private LevelObjectData objectData;
    [SerializeField] private GameObject coin;

    private int health = 1;
    private Rigidbody2D rigidbody;

    private void Start()
    {
        health = objectData.Health;
        rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.bodyType = objectData.IsStatic ? RigidbodyType2D.Static : RigidbodyType2D.Dynamic;
    }

#if UNITY_EDITOR

    [ContextMenu("Rename")]
    private void Rename()
    {
        if(objectData != null)
            gameObject.name = objectData.Name;
    }

    [ContextMenu("Move Right")]
    private void MoveRight()
    {
        Move(Vector2.right);
    }

    [ContextMenu("Move Left")]
    private void MoveLeft()
    {
        Move(Vector2.left);
    }

    [ContextMenu("Add Top")]
    private void AddTop()
    {
        var obj = Instantiate(gameObject);

        var size = obj.GetComponent<Collider2D>().bounds.size.x;

        obj.transform.Translate(Vector2.up * size);
    }

    private void Move(Vector2 direction)
    {
        var collider = GetComponent<Collider2D>();
        var size = collider.bounds.size.x;

        transform.Translate(direction * size);
    }

#endif

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0) Die();
        }
    }

    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        Destroy(gameObject);

        for (int i = 0; i < 5; i++)
        {
            var obj = Instantiate(coin);

            obj.transform.position = transform.position;

            var rigidbody = obj.GetComponent<Rigidbody2D>();
            rigidbody.bodyType = RigidbodyType2D.Dynamic;

            rigidbody.AddForce(new Vector2(Random.Range(-2f, 2f), 3f), ForceMode2D.Impulse);

            obj.AddComponent<BoxCollider2D>();
        }
    }
}

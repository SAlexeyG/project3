﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class StarBonus : MonoBehaviour
{
    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private float effectLifeTime = 1f;
    [SerializeField] private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        particleSystem.Stop();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.transform.root.GetComponent<Player>();

        if(player != null)
        {
            spriteRenderer.enabled = false;
            particleSystem.Play();

            Destroy(gameObject, effectLifeTime);
        }
    }
}

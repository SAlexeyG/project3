﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	private void OnTriggerEnter2D(Collider2D collision)
	{
		var player = collision.gameObject.GetComponent<Player>();
		if (player != null)
		{
			GameManager.Coins++;
			StartCoroutine(MoveUp());
			GetComponent<Collider2D>().enabled = false;
		}
	}

	private IEnumerator MoveUp()
	{
		gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;

		var box = gameObject.GetComponent<BoxCollider2D>();
		if (box != null) box.enabled = false;

		var timer = 1f;

		while (timer > 0f)
		{
			transform.Translate(Vector2.up * Time.deltaTime);
			timer -= Time.deltaTime;
			yield return null;
		}

		Destroy(gameObject);
	}
}

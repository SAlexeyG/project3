﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockFall : MonoBehaviour
{
    [SerializeField] private Sprite[] rocks;
    [SerializeField] private float distance;
    [SerializeField] PhysicsMaterial2D physicMaterial;

    private IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.7f);

            var obj = new GameObject();

            var renderer = obj.AddComponent<SpriteRenderer>();
            renderer.sprite = rocks[Random.Range(0, rocks.Length)];
            renderer.sortingLayerName = "Enviroment";

            obj.transform.position = new Vector2(Random.Range(this.transform.position.x - distance, this.transform.position.x + distance), transform.position.y);

            var rigidbody = obj.AddComponent<Rigidbody2D>();
            rigidbody.sharedMaterial = physicMaterial;

            var collider = obj.AddComponent<CircleCollider2D>();
            collider.sharedMaterial = physicMaterial;

            obj.AddComponent<Rock>();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using UnityEngine;

public enum GameState
{
	MainMenu,
	Loading,
	Game,
	GamePause,
}

public class GameManager : MonoBehaviour
{
	private static GameState cuttentGameState;
	public static GameState CurrentGameState => cuttentGameState;

	public static Action<GameState> GameStateAction;

	public IPlayer Player;
	public List<IEnemy> Enemies = new List<IEnemy>();

	private static int coins;

	public static int Coins
	{
		get => coins;
		set
		{
			coins = value;
			Debug.Log($"Coins: {coins}");
		}
	}

	public static void SetGameState(GameState state)
	{
		cuttentGameState = state;
		GameStateAction?.Invoke(state);
	}

	private void Start()
	{
		SetGameState(GameState.Game);
		print(Player);
		print($"{Enemies.Count} - enemies");
	}

#if UNITY_EDITOR
	[ContextMenu("Hide all characters")]
	public void HideAllCharecters()
	{
		var characters = FindCharacters();
		foreach (var character in characters)
			character.SetActive(false);
	}

	[ContextMenu("Show all characters")]
	public void ShowAllCharecters()
	{
		var characters = FindCharacters();
		foreach (var character in characters)
			character.SetActive(true);
	}

	private List<GameObject> FindCharacters()
	{
		var characters = Resources.FindObjectsOfTypeAll<Enemy>().
			Select(e => e.gameObject).ToList();

		var players = Resources.FindObjectsOfTypeAll<Player>().
			Select(e => e.gameObject).ToList();

		characters.AddRange(players);

		return characters;
	}

#endif
}

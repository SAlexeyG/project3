﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class KnightLoading : MonoBehaviour
{
    Color[] colors = new Color[] 
    { 
        new Color(1f, 1f, 1f, 1f), 
        new Color(1f, 0.8745098f, 0.4862745f, 1f), 
        new Color(0.4705882f, 1f, 0.8941177f, 1f), 
        new Color(1f, 0.4705882f, 0.7137255f, 1f) 
    };

    float time = 0f;
    int colorID = 1;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > 1f)
        {
            gameObject.GetComponent<UnityEngine.UI.Image>().color = colors[colorID];
            colorID = (colorID + 1) % colors.Length;

            time -= 2;
        }
    }
}

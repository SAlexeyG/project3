﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct EffectLifeComponent : IComponentData
{
    public float LifeTime;
}

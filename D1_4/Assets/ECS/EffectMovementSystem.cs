﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

public class EffectMovementSystem : JobComponentSystem
{
    private float lastUpdateTime;
    
    protected override JobHandle OnUpdate(JobHandle jobHandle)
    {
        var deltaTime = World.Time.DeltaTime;

        Entities.WithoutBurst().WithStructuralChanges().ForEach(
            (ref Entity entity, ref EffectLifeComponent component) =>
            {
                component.LifeTime -= deltaTime;
                if(component.LifeTime <= 0) 
                    EffectController.EntityManager.DestroyEntity(entity);
            }).Run();

        return default;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.VersionControl;
using UnityEngine;

public class CreateAssetBundles
{
    [MenuItem("Asset Bundles/Build")]
    private static void BuildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None,
            BuildTarget.StandaloneWindows64);

        AssetDatabase.Refresh();
    }

    [MenuItem("Asset Bundles/Get names")]
    private static void GetNames()
    {
        var names = AssetDatabase.GetAllAssetBundleNames();
        foreach (var name in names)
        {
            Debug.Log($"Asset bundle name: {name}");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.Networking;

public class EffectsLoader : MonoBehaviour
{
    private AssetBundle bundle;

    private IEnumerator Start()
    {
        using (UnityWebRequest webRequest = UnityWebRequestAssetBundle.GetAssetBundle(
            "https://drive.google.com/uc?export=download&id=1IC1nWbEYsk5-jl203-OGwl1isP4EFAjk"))
        {
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.LogError(webRequest.error);
            }
            else
            {
                bundle = DownloadHandlerAssetBundle.GetContent(webRequest);
                Debug.Log($"Bundle {bundle.name} has loaded");
            }
        }
    }

#if UNITY_EDITOR

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            LoadEffect("Effect1");
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            LoadEffect("Effect2");
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            LoadEffect("Effect3");
        }
        
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha1))
        {
            LoadEffectFromBundle("Effect1");
        }
        
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha2))
        {
            LoadEffectFromBundle("Effect2");
        }
        
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha3))
        {
            LoadEffectFromBundle("Effect3");
        }
    }
    
    #endif

    public GameObject LoadEffect(string name)
    {
        var path = $"Effects/{name}";
        var prefab = Resources.Load<GameObject>(path);
        return prefab ? Instantiate(prefab) : null;
    }

    private void LoadEffectFromBundle(string name)
    {
        if (bundle == null)
        {
            bundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "effects"));
            Debug.Log("Bundle is loaded from hard drive");
        }

        if (bundle == null)
        {
            Debug.Log("Failed to load asset bundle");
            return;
        }

        var prefab = bundle.LoadAsset<GameObject>(name);
        Instantiate(prefab);
        bundle.Unload(false);
    }
}

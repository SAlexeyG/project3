﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{
    [SerializeField] private Image lifeProgress;

    [SerializeField] private ParticleSystem downParticleSystem;
    [SerializeField] private float maxDownEmitter = 1.2f;
    [SerializeField] private Transform upPoint;
    [SerializeField] private Transform downPoint;

    // Start is called before the first frame update
    void Start()
    {
        lifeProgress.fillAmount = 1f;
        downParticleSystem.Stop();
        downParticleSystem.transform.position = upPoint.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            SetLife(0.1f);
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
            SetLife(-0.1f);
    }

    private async void SetLife(float value)
    {
        var dir = value > 0 ? 1 : -1;
        var target = Mathf.Abs(value);

        if (dir < 0) downParticleSystem.Play();

        var particleTransform = downParticleSystem.transform;
        var pos = particleTransform.position;

        while (target > 0f)
        {
            target -= Time.deltaTime;
            var fillAmmount = lifeProgress.fillAmount;
            fillAmmount += Time.deltaTime * dir;
            lifeProgress.fillAmount = fillAmmount;

            var y = Mathf.Lerp(downPoint.position.y, upPoint.position.y, fillAmmount);
            pos.y = y;

            particleTransform.position = pos;
        }
    }
}

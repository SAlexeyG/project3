﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCar : MonoBehaviour
{
    [SerializeField] private Transform centerOfMass;

    [SerializeField] private float maxTorque = 500f;
    [SerializeField] private float maxSteerAngle = 45f;

    [SerializeField] private Transform[] wheels;
    [SerializeField] private WheelCollider[] wheelColliders;

    private Rigidbody rig;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
        rig.centerOfMass = centerOfMass.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMeshPositions();
    }

    private void FixedUpdate()
    {
        float steer = Input.GetAxis("Horisontal") * maxSteerAngle;

        float torque = Input.GetAxis("Vertical") * maxTorque;

        wheelColliders[0].steerAngle = steer;
        wheelColliders[1].steerAngle = steer;

        for (int i = 0; i < 4; i++)
        {
            wheelColliders[i].motorTorque = torque;
        }
    }

    private void UpdateMeshPositions()
    {
        
    }
}

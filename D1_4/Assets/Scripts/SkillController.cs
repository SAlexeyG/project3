﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillController : MonoBehaviour
{
    [Header("Main Data")]
    [SerializeField] private int damage = 5;
    [SerializeField] private float speed = 2f;
    
    [Header("Main Sphere")]
    [SerializeField] private Transform mainSphere;
    [SerializeField] private AnimationCurve mainPeriodAnimationCurve;
    [SerializeField] private float mainPeriodTime = 2f;

    private float mainPeriod;
    
    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime * Vector3.forward, Space.World);

        if (mainPeriod > 1f) mainPeriod = 1f;

        var scale = mainPeriodAnimationCurve.Evaluate(mainPeriod);
        mainSphere.localScale = Vector3.one * scale;
        mainPeriod += Time.deltaTime / mainPeriodTime;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
	[SerializeField] private HopPlayer m_Player;
	[SerializeField] private GameObject m_Platform;
	[SerializeField] private GameObject m_BoostPlatform;
	[SerializeField] private GameObject m_JumpPlatform;
	[SerializeField] private GameObject m_NewPlatform;

	private List<GameObject> platforms = new List<GameObject>();
	// Start is called before the first frame update
	void Start()
	{
		//Генерация платформ
		platforms.Add(m_Platform.gameObject);
		for (int i = 0; i < 25; i++)
		{
			GameObject obj;

			int chance = Random.Range(0, 101);
			if (chance >= 50)
			{
				obj = Instantiate(m_Platform, transform);
				obj.name = $"Platform {i}";
			}
			else if(chance >= 15)
			{
				obj = Instantiate(m_BoostPlatform, transform);
				obj.name = $"BoostPlatform {i}";
			}
			else
			{
				obj = Instantiate(m_JumpPlatform, transform);
				obj.name = $"JumpPlatform {i}";
			}

			Vector3 pos = Vector3.zero;

			pos.z = 2 * (i + 1);
			pos.x = Random.Range(-1, 2);
			obj.transform.position = pos;

			platforms.Add(obj);
		}
	}

	public bool IsBallOnPlatform(Vector3 position)
	{
		position.y = 0f;

		GameObject nearest = platforms[0];

		for (int i = 0; i < platforms.Count; i++)
		{
			if (platforms[i].transform.position.z + 0.5f < position.z)
				continue;

			if (platforms[i].transform.position.z - position.z > 1f)
				continue;

			nearest = platforms[i];
			break;
		}

		float minX = nearest.transform.position.x - 0.5f;
		float maxX = nearest.transform.position.x + 0.5f;

		if (position.x > minX && position.x < maxX)
		{
			m_NewPlatform.transform.position = nearest.transform.position;
			nearest.SetActive(false);

			if (nearest.name.Contains("BoostPlatform"))
			{
				m_Player.AddBoost();
				m_Player.RemoveJump();
			}
			else if (nearest.name.Contains("JumpPlatform"))
			{
				m_Player.AddJump();
				m_Player.RemoveBoost();
			}
			else
			{
				m_Player.RemoveBoost();
				m_Player.RemoveJump();
			}

			return true;
		}
		else
			return false;
	}
}

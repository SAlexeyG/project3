using DefaultNamespace;
using System.Runtime.ExceptionServices;
using UnityEngine;

public class FitInTheHole_LevelGenerator : MonoBehaviour
{
	[SerializeField] private GameObject m_CubePrefab;
	[SerializeField] private float m_BaseSpeed = 2f;
	[SerializeField] private float m_WallDistance = 35f;

	private float speed;
	private FitInTheHole_Wall wall;
	private bool IsGameOver = false;

	[SerializeField] private FitInTheHole_Template[] m_Templates;
	[SerializeField] private Transform m_FigurePoint;
	[SerializeField] private Transform m_RedCube;

	private FitInTheHole_Template[] templates;
	private FitInTheHole_Template figure;

	private void Start()
	{
		templates = new FitInTheHole_Template[m_Templates.Length];

		for (int i = 0; i < m_Templates.Length; i++)
		{
			templates[i] = Instantiate(m_Templates[i]);
			templates[i].gameObject.SetActive(false);
			templates[i].transform.position = m_FigurePoint.position;
		}

		wall = new FitInTheHole_Wall(5, 5, m_CubePrefab);
		SetupTemplate();
		wall.SetupWall(figure, m_WallDistance);
		speed = m_BaseSpeed;
	}

	private void SetupTemplate()
	{
		if (figure != null)
		{
			figure.gameObject.SetActive(false);
		}

		var rand = Random.Range(0, templates.Length);
		figure = templates[rand];
		figure.gameObject.SetActive(true);
		figure.SetupRandomFigure();
	}

	private void Update()
	{
		wall.Parent.transform.Translate(Vector3.back * (Time.deltaTime * speed));

		if (wall.Parent.transform.position.z < m_FigurePoint.position.z && wall.Parent.transform.position.z > m_FigurePoint.position.z - 0.08f)
			if (figure.CurrentTarget.position != figure.PlayerPosition.position)
			{
				speed = 0.5f;
				m_RedCube.position = figure.PlayerPosition.position;
				m_RedCube.Translate(new Vector3(0f, 0f, -0.1f));
				IsGameOver = true;
				Debug.Log("Game over");
			}

		if (IsGameOver) m_RedCube.Translate(Vector3.back * (Time.deltaTime * speed));

		if (wall.Parent.transform.position.z > -m_WallDistance) return;

		speed++;
		SetupTemplate();
		wall.SetupWall(figure, m_WallDistance);
	}
}
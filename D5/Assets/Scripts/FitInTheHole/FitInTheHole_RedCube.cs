﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_RedCube : MonoBehaviour
{
    float time = 0f;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if (time > 0.5f)
        {
            time -= 0.5f;
            gameObject.GetComponent<MeshRenderer>().enabled = !gameObject.GetComponent<MeshRenderer>().enabled;
        }
    }
}
